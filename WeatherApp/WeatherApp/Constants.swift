//
//  Constants.swift
//  WeatherApp
//
//  Created by Tuong Nguyen on 01/11/2016.
//  Copyright © 2016 Tuong Nguyen. All rights reserved.
//

import Foundation

let BASE_URL = "http://api.openweathermap.org/data/2.5/weather?"
let LATITUDE = "lat="
let LONGITUDE = "&lon="
let APP_ID = "&appid="
let API_KEY = "bac10226f5a5df8a2792683857980d63"

let CURRENT_WEATHER_URL = "\(BASE_URL)\(LATITUDE)10.8288564\(LONGITUDE)106.701622\(APP_ID)\(API_KEY)"

typealias DownloadCompelete = () -> ()
